import React, {useReducer, useEffect} from 'react';
import {SELECTED_SHOW} from "../../store/ actionTypes";
import {infoSeries} from "../../store/actions";
import instanceByID from "../../axiosID";
import ReactHtmlParser from 'react-html-parser';

const initialState = {
    selectedShow: ""
};

const reducer = (state, action) => {
    switch (action.type) {
        case SELECTED_SHOW:
            return {
                ...state,
                selectedShow: action.tvShow
            }
        default:
            return state;
    };
};

const Show = props => {

    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const fetchData = async () => {
            const response = await instanceByID.get(props.match.params.id);
            dispatch(infoSeries(response.data));
        };
        fetchData().catch(console.error);
    }, [props.match.params.id]);

    let summary;
    if (state.selectedShow) {
        const html = state.selectedShow.summary;
        summary = ReactHtmlParser(html)
    }

    let poster;
    if (state.selectedShow.image) {
        poster = <img src={state.selectedShow.image.original} className="card-img-centre float-left mr-3"
                      style={{width: 300}} alt="show"/>
    }

    return state.selectedShow && (
        <div className="container bg-info mt-3">
            <h1 className="text-light">{state.selectedShow.name}</h1>
            <div className="card">
                <div className="card-body">
                    {poster}
                    <p className="card-text text-secondary"><b>Type:</b> {state.selectedShow.type}</p>
                    <p className="card-text text-secondary"><b>Language:</b> {state.selectedShow.language}</p>
                    <p className="card-text text-secondary"><b>Status:</b> {state.selectedShow.status}</p>
                    <h3 className="text-secondary mt-3">This TV-Show is about:</h3>
                    <p className="card-text text-secondary">{summary}</p>
                </div>

            </div>
        </div>
    );

};

export default Show;