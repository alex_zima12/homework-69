import React, {useReducer, useEffect} from 'react';
import {saveShow, fetchSuccess} from "../../../store/actions";
import {FETCH_SUCCESS, SAVE_CURRENT_SHOW} from "../../../store/ actionTypes";
import instance from "../../../axios-shows";
import {NavLink} from "react-router-dom";

const initialState = {
    currentShow: '',
    shows: ''
};

const reducer = (state, action) => {
    switch (action.type) {
        case SAVE_CURRENT_SHOW:
            return {
                ...state,
                currentShow: action.currentShow
            };
        case FETCH_SUCCESS:
            return {
                ...state,
                shows: action.show
            };
        default:
            return state;
    };
}

const Toolbar = props => {

    const [state, dispatch] = useReducer(reducer, initialState);

    const currentShow = (event) => {
        const currentShow = event.target.value
        dispatch(saveShow(currentShow));
    };

    useEffect(() => {
        if (state.currentShow) {
            const fetchData = async () => {
                const response = await instance.get(state.currentShow)
                dispatch(fetchSuccess(response.data));
            };
            fetchData().catch()
        };
    }, [dispatch, state.currentShow]);

    let array = [];
    if (state.shows) {
        array = state.shows.map(show => {
            return <NavLink key={show.show.id} className="list-group-item" to={"/show/"+show.show.id}>{show.show.name}</NavLink>
        })
    };

    return (
        <>
            <nav className="navbar navbar-dark bg-primary ">
                <p className="font-weight-bold text-light container">TV Shows</p>
            </nav>
            <div>
                <input onChange={currentShow} type="text" className="form-control container mt-3" placeholder="Enter TV shows"/>
                <ul className="list-group container">
                    {array}
                </ul>
            </div>
        </>
    );
};

export default Toolbar;