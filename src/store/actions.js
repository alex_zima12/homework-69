import {
    SAVE_CURRENT_SHOW,
    SELECTED_SHOW,
    FETCH_SUCCESS
} from "./ actionTypes";


export const saveShow = currentShow => (
    {type: SAVE_CURRENT_SHOW,currentShow}
);

export const fetchSuccess = show => (
    {type: FETCH_SUCCESS, show}
);

export const infoSeries = (tvShow) => {
    return {type: SELECTED_SHOW, tvShow}
};
