import React from 'react';
import {Route, Switch} from 'react-router-dom';
import './App.css';
import Layout from "./components/Layout/Layout";
import Show from "./components/Show/Show";

function App() {
    return (
        <Layout>
            <Switch>
                <Route path="/show/:id" component={Show}/>
                <Route render={() => <h1 className="container mt-3 text-muted"> Search for TV shows </h1>}/>
            </Switch>
        </Layout>
    );
}

export default App;
