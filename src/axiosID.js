import axios from 'axios';

const instanceByID = axios.create({
    baseURL: 'http://api.tvmaze.com/shows/'
});

export default instanceByID;